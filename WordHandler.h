﻿#pragma once
#include <string>
#include <vector>

using namespace std;

class WordHandler
{
private:
    string SubsPath = "../Subtitles";
    string WordsPath = "../Generated Words";
    string PersonalDictonaryPath = WordsPath + "/PersonalDictionary.txt";
    string OxfordWordsPath = WordsPath + "/OxfordBaseWords.txt";
    string SubsWordsPath = WordsPath + "/NewWordsFromSubtitles.txt";
    string PopularWordsPath = WordsPath + "/MostPopularWords.txt";

    vector<string> WordsFromSubs;
    vector<string> PersonalDictonary;
    vector<string> OxfordDictonary;
    int WordIndex = 0;
    
    bool IsConsistText(const std::string& str);
    bool IsConsistSymbol(const char str);

    void AddNewWord(std::string Word);

    void UpdateDictonaries();
    void GetDictonary(vector<string>& array, std::string path);

    void GetSubtitles();
    void GetDividedWordsFromFile(std::ifstream& CurrentFile);
    void GetWordsFromOxfordDictonary();
    
    void ChangeCase(std::string& str);
    void Write(vector<string> TextFrom, string path);


public:
    WordHandler();
};
