﻿#include "WordHandler.h"

#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <array>
#define _SILENCE_EXPERIMENTAL_FILESYSTEM_DEPRECATION_WARNING 1;
#include <experimental/filesystem>

using namespace std;
namespace FileSystem = experimental::filesystem;

WordHandler::WordHandler()
{
    cout << "Working...\n";
    
    UpdateDictonaries();
    GetSubtitles();
    GetWordsFromOxfordDictonary();
    
    cout << "Succeeded\n" << "Press any key to exit\n";
}

void WordHandler::UpdateDictonaries()
{
    GetDictonary(PersonalDictonary, PersonalDictonaryPath);
    GetDictonary(OxfordDictonary, OxfordWordsPath);
    
    vector<string> temp; 
     for (auto value : OxfordDictonary)
        {
           if(!binary_search(PersonalDictonary.begin(), PersonalDictonary.end(), value))
           {
               temp.push_back(value);
           }
        }

    OxfordDictonary.clear();
    OxfordDictonary = move(temp);
    Write(OxfordDictonary, OxfordWordsPath);
}

void WordHandler::GetDictonary(vector<string>& array, string path)
{
    ifstream CurrentFile(path);
    string NextLine;
    
    int size = 0;
    string* arr = new string[10000];

    while (getline(CurrentFile, NextLine, '\n'))
    {
        ChangeCase(NextLine);
        arr[size++] = NextLine;
    }
    
    for (int i = 0; i < size - 1; i++)
    {
        array.push_back(arr[i]);
    }

    sort(array.begin(), array.end());
    delete[] arr;
    arr = nullptr;
}

void WordHandler::GetSubtitles()
{
    for (const auto& File : FileSystem::directory_iterator(SubsPath))
    {
        std::cout << "File:" << File.path() << std::endl;

        ifstream CurrentFile(File);
        GetDividedWordsFromFile(CurrentFile);
    }

   Write(WordsFromSubs, SubsWordsPath);
}

void WordHandler::GetDividedWordsFromFile(ifstream& CurrentFile)
{
    string Line;
    while (getline(CurrentFile, Line, '\n'))
    {
        ChangeCase(Line);

        if (IsConsistText(Line))
        {
            string Word;
            
            for (const char Letter : Line)
            {
                if (IsConsistSymbol(Letter))
                {
                    if (Word.length() > 2)
                    {
                        
                        AddNewWord(Word);
                    }

                    Word = "";
                }
                
                else
                {
                    Word += Letter;
                }
                
            }
        }
    }
    
    sort(WordsFromSubs.begin(), WordsFromSubs.end());
}

void WordHandler::ChangeCase(std::string& str)
{
    for (int i = 0; i < str.length(); i++)
    {
        str[i] = tolower(str[i]);
    }
}

void WordHandler::AddNewWord(string Word)
{
    if (find(WordsFromSubs.begin(), WordsFromSubs.end(), Word) != WordsFromSubs.end() || find(PersonalDictonary.begin(), PersonalDictonary.end(), Word) != PersonalDictonary.end())
    {
        return;
    }
    
    WordsFromSubs.push_back(Word);
}

void WordHandler::Write(vector<string> TextFrom, string path)
{
    ofstream Writer;
    Writer.open(path);

    for (auto Line : TextFrom)
    {
        Writer << Line << endl;
    }
}

bool WordHandler::IsConsistText(const std::string& str)
{
    return str.find_first_of("0123456789") == string::npos;
}

bool WordHandler::IsConsistSymbol(const char str)
{
    char symbols[] = {'!', '.', '?', '"', '-', ',', ' ', '<', '>', ':', '[', ']', '{', '}', '”', '“'};
    return find(begin(symbols), end(symbols), str) != end(symbols);
}

void WordHandler::GetWordsFromOxfordDictonary()
{
    vector<string> PopularWords;
    
    for (int i = 0, j = 0; i < 100;)
    {
        if (OxfordDictonary.size() <= j)
        {
            break;
        }
        if(find(WordsFromSubs.begin(), WordsFromSubs.end(), OxfordDictonary[j]) != WordsFromSubs.end())
        {
            PopularWords.push_back(OxfordDictonary.at(j++));
            i++; 
        }
        else
        {
            j++;
        }
    }

    Write(PopularWords, PopularWordsPath);
}
